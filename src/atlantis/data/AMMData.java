package atlantis.data;

import atlantis.event.*;
import atlantis.graphics.ACoord;
import atlantis.output.ALogInterface;
import atlantis.output.AOutput;
import atlantis.parameters.AParameterUtilities;
import atlantis.utils.AAtlantisException;
import atlantis.utils.AHashMap;
import atlantis.utils.AIdHelper;
import atlantis.utils.ALogger;
import atlantis.utils.AMath;
import atlantis.projection.AProjectionYX;

/**
  * The New Small Wheel Micromegas (MM)
  *
  * @author Liaoshan Shi
  */

public class AMMData extends AMuonHitData
{
    float[] length;
    float[] angle;
    int[] gasGap;

    private static ALogger logger = ALogger.getLogger(AMMData.class);    

    public String getParameterGroup()
    {
        return "MM";
    }

    public String getName()
    {
        return "MM";
    }

    public String getNameScreenName()
    {
        return "MM";
    }

    AMMData(AHashMap p, AEvent e)
    {
        super(p,e);
        length = p.getFloatArray("length");
        angle = p.getFloatArray("angle");
        gasGap = new int[numData];
        for (int i = 0; i < numData; i++)
            gasGap[i] = getGasGap(i);
    }

    protected int getStation(int index)
    {
        return 0;
    }

    protected int getSub(int index)
    {
        try {
            if (AIdHelper.stationEta(id[index]) < 0) {
                return 0;
            } else {
                return 1;
            }

        } catch (AAtlantisException e) {
            logger.error("Problem decoding MM identifier", e);
        }
        
        return 0;
    }

    public int getSector(int index)
    {
        try {
            String stationName = AIdHelper.stationName(id[index]);

            if (stationName.charAt(2) == 'L') {
                return 2 * (AIdHelper.stationPhi(id[index]) - 1);
            } else {
                return 2 * (AIdHelper.stationPhi(id[index]) - 1) + 1;
            }
        } catch (AAtlantisException e) {
            logger.error("Problem decoding MM identifier", e);
        }

        return 0;
    }

    protected int getGasGap(int index)
    {
        try {
            return AIdHelper.mmGasGap(id[index]);
        } catch (AAtlantisException e) {
            logger.error("Problem decoding MM identifier", e);
        }
        
        return 0;
    }

    protected boolean getMeasuresPhi(int index)
    {
        return false;
    }

    public void applyCuts()
    {
        super.applyCuts();
        if (parameterStore.get("CutsATLAS", "CutPhi").getStatus())
            cutPhi(phi, getDPhi());
        cutEta(rho, z);
    }

    // used for cuts

    private float[] getDPhi()
    {
        // only roughly correct
        // must create all
        float[] dphi = new float[numData];

        // need only fill for those in draw list
        for (int i = 0; i < numDraw; i++)
        {
            int list = listdl[i];

            dphi[list] = (float) Math.abs(Math.atan2( ( length[list] * Math.cos(angle[list]) ) / 2., rho[list]));
        }
        return dphi;
    }

    // used for cuts

    protected ACoord getFZUser()
    {
        makeDrawList();
        double[][][] hv = new double[2][2][numDraw];
        int[] index = new int[numDraw];

        for (int i = 0; i < numDraw; i++)
        {
            int list = listdl[i];

            index[i] = list;
            double deltaPhi = ( ( length[list] * Math.cos(angle[list]) ) / 2.) / rho[list];

            hv[0][0][i] = z[list];
            hv[1][0][i] = Math.toDegrees(phi[list] - deltaPhi);
            hv[0][1][i] = z[list];
            hv[1][1][i] = Math.toDegrees(phi[list] + deltaPhi);
        }
        return new ACoord(hv, index, this, ACoord.LINES).includePhiWrapAround("FZ");
    }

    private void makeDrawListYX()
    {
        int mode = parameterStore.get("YX", "Mode").getI();

        if (mode != AProjectionYX.MODE_NSW_MM)
        {
            numDraw = 0;
        }
        else
        {
            makeDrawList();
        }
        cut("YX", "MMGasGap", " MM Gas Gap", gasGap);
    }

    protected ACoord getYXUser()
    {
        makeDrawListYX();

        double[][][] hv = new double[2][numDraw][2];
        int[] index = new int[numDraw];

        for (int i = 0; i < numDraw; i++)
        {
            int list = listdl[i];

            index[i] = list;
            double cosPhi = Math.cos(phi[list]);
            double sinPhi = Math.sin(phi[list]);
            double rMid = rho[list];
            double r = rho[list];
            double d = length[list] / 2.;
            double x = r * cosPhi;
            double y = r * sinPhi;
            double dx = d * Math.sin(phi[list] + angle[list]);
            double dy = d * Math.cos(phi[list] + angle[list]);

            hv[0][i][0] = x + dx;
            hv[1][i][0] = y - dy;
            hv[0][i][1] = x - dx;
            hv[1][i][1] = y + dy;
        }
        return new ACoord(hv, index, this);
    }

    protected ACoord getFRUser()
    {
        return getYXUser().convertYXToFR().includePhiWrapAround("FR");
    }

    protected ACoord getXZRZUser(int sign[]) 
    {
        double[][][] hv = new double[2][numDraw][];
        int index[] = new int[numDraw];

		for (int i=0; i<numDraw; i++) {
		   int j = listdl[i];
		   index[i] = j;

		   double rho = Math.sqrt(x[j]*x[j] + y[j]*y[j]);

		   hv[0][i] = new double[] { z[j] };
		   hv[1][i] = new double[] { sign[i] * rho };
		}

		return new ACoord(hv, index, this);
    }

    protected ACoord getXZUser() {
        makeDrawList();
        cutMuonSector(sector);
        
        int[] sign = new int[numDraw];
        int sect = (int) Math.round(parameterStore.get("XZ", "Phi").getD() / 22.5);
        
        for(int i=0; i<numDraw; i++) {
            if (sector[listdl[i]] == sect)
                sign[i] = 1;
            else
                sign[i] = -1;
        }
        
        return getXZRZUser(sign);
    }
    
    protected ACoord getRZUser() {
        makeDrawList();
        
        int[] sign = new int[numDraw];
        for (int i=0; i<numDraw; i++) {
            int j = listdl[i];
            sign[i] = AParameterUtilities.getRhoSign(x[j], y[j]);
        }
        
        return getXZRZUser(sign);
    }
}
