package atlantis.data;

import atlantis.event.*;
import atlantis.projection.AProjectionYX;
import atlantis.utils.AHashMap;
import atlantis.utils.AIdHelper;
import atlantis.utils.AAtlantisException;
import atlantis.utils.ALogger;

/**
 *
 * @author Eric Jansen
 */
public class AMuonSegmentData extends ASegmentData {
    
    private static ALogger logger = ALogger.getLogger(ATGCData.class);

    /** Creates a new instance of AMuonSegmentData */
    AMuonSegmentData(AHashMap p, AEvent e) {
        super(p,e);
        String assocKey = getName() + getStoreGateKey();
        event.getAssociationManager().add(new AAssociation(assocKey, "MDT", numHits, hits, event));
        event.getAssociationManager().add(new AAssociation(assocKey, "RPC", numHits, hits, event));
        event.getAssociationManager().add(new AAssociation(assocKey, "TGC", numHits, hits, event));
        event.getAssociationManager().add(new AAssociation(assocKey, "CSC", numHits, hits, event));
        event.getAssociationManager().add(new AAssociation(assocKey, "STGC", numHits, hits, event));
        event.getAssociationManager().add(new AAssociation(assocKey, "MM", numHits, hits, event));

    }

    public void makeDrawList() {
        super.makeDrawList();

        if (currentProjection instanceof AProjectionYX) {
            if (parameterStore.get("YX", "Mode").getI() != AProjectionYX.MODE_STANDARD) {
                numDraw = 0;
            } else {
                int [][] hitMap = reshapeHits(hits, numHits);
                int num = 0;
                for (int i=0; i<numDraw; i++) {
                    int j = listdl[i];

                    // Only draw the Muon segment if it goes across at least one barrel chamber
                    if (isBarrel(j, hitMap)) {
                        listdl[num++] = j;
                    }

                }
                numDraw = num;
            }
        }
    }

    public String getParameterGroup() {
        return "MuonSegment";
    }
    
    public String getName() {
        return "MuonSegment";
    }
    
    public String getNameScreenName() {
        return "MuonSegment";
    }    

    /**
      * Reshape the hits from a 1D array to a 2D array.
      * The first index is the segment index. The second index is the hit index in each segment.
      * @param hits the Muon hits in a 1D int array
      * @param numHigs the number of Hits in each segment in a 1D int array
      * @return the Muon hits in a 2D int array
      */
    private int[][] reshapeHits(int[] hits, int[] numHits) {
        int [][] assoc = null;
        if (numHits != null && hits != null) {
            assoc = new int[numHits.length][];
            int num = 0;
            for (int i = 0; i < numHits.length; ++i) {
                if (numHits[i] <= 0) {
                    assoc[i] = new int[0];
                } else {
                    assoc[i] = new int[numHits[i]];
                }
                for (int j=0; j<numHits[i]; ++j) {
                    assoc[i][j] = hits[num++];
                }
            }
        }
        return assoc;
    }

    /**
      * Check if a Muon segment belongs to barrel.
      * This is done by checking the identifiers of the hits associated to the Muon segment. 
      * If any of the hits is in a barrel Muon chamber (whose name starts with "B"), the segment
      * is considered to be in the barrel. Otherwise the segment is in the endcap.
      * @param iSegment index of the Muon segment
      * @param hitMap the Muon hits in a 2D int array
      * @return a boolean
      */
    private boolean isBarrel(int iSegment, int[][] hitMap){
        boolean isBarrel = false;
        for (int iHit=0; iHit<hitMap[iSegment].length; iHit++) {
            try{
                String stationName = AIdHelper.stationName(hitMap[iSegment][iHit]);
                if (stationName.charAt(0) == 'B') {
                    isBarrel = true;
                }
            } catch (AAtlantisException e) {
                logger.error("Problem decoding identifier for Muon segment hits", e);
            }
        }

        return isBarrel;

    }
}
