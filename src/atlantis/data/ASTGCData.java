package atlantis.data;

import atlantis.event.*;
import atlantis.graphics.ACoord;
import atlantis.output.ALogInterface;
import atlantis.output.AOutput;
import atlantis.parameters.AParameterUtilities;
import atlantis.utils.AAtlantisException;
import atlantis.utils.AHashMap;
import atlantis.utils.AIdHelper;
import atlantis.utils.ALogger;
import atlantis.utils.AMath;
import atlantis.projection.AProjectionYX;

/**
  * The New Small Wheel small-strip Thin Gap Chambers (sTGC)
  *
  * @author Liaoshan Shi
  */

public class ASTGCData extends AMuonHitData
{
    float[] swidth;
    float[] lwidth;
    float[] length;
    int[] gasGap;

    private static ALogger logger = ALogger.getLogger(ASTGCData.class);    

    public String getParameterGroup()
    {
        return "STGC";
    }

    public String getName()
    {
        return "STGC";
    }

    public String getNameScreenName()
    {
        return "STGC";
    }

    ASTGCData(AHashMap p, AEvent e)
    {
        super(p,e);
        swidth = p.getFloatArray("swidth");
        length = p.getFloatArray("length");
        lwidth = p.getFloatArray("lwidth");
        gasGap = new int[numData];
        for (int i = 0; i < numData; i++)
            gasGap[i] = getGasGap(i);
    }

    protected int getStation(int index)
    {
        return 0;
    }

    protected int getSub(int index)
    {
        try {
            if (AIdHelper.stationEta(id[index]) < 0) {
                return 0;
            } else {
                return 1;
            }

        } catch (AAtlantisException e) {
            logger.error("Problem decoding STGC identifier", e);
        }
        
        return 0;
    }

    public int getSector(int index)
    {
        try {
            String stationName = AIdHelper.stationName(id[index]);

            if (stationName.charAt(2) == 'L') {
                return 2 * (AIdHelper.stationPhi(id[index]) - 1);
            } else {
                return 2 * (AIdHelper.stationPhi(id[index]) - 1) + 1;
            }
        } catch (AAtlantisException e) {
            logger.error("Problem decoding STGC identifier", e);
        }

        return 0;
    }

    protected int getGasGap(int index)
    {
        try {
            return AIdHelper.stgcGasGap(id[index]);
        } catch (AAtlantisException e) {
            logger.error("Problem decoding STGC identifier", e);
        }
        
        return 0;
    }

    protected boolean getMeasuresPhi(int index)
    {
        try {
            if (AIdHelper.stgcChannelType(id[index]) == 1) {
                return false;
            }
        } catch (AAtlantisException e) {
            logger.error("Problem decoding STGC identifier", e);
        }
        
        return true;
    }

    public void applyCuts()
    {
        super.applyCuts();
        if (parameterStore.get("CutsATLAS", "CutPhi").getStatus())
            cutPhi(phi, getDPhi());
        if (parameterStore.get("CutsATLAS", "CutEta").getStatus())
            cutEtaDRho(rho, z, getDRho());
    }

    // used for cuts

    private float[] getDPhi()
    {
        // only roughly correct
        // must create all
        float[] dphi = new float[numData];

        // need only fill for those in draw list
        for (int i = 0; i < numDraw; i++)
        {
            int list = listdl[i];

            dphi[list] = (float) Math.abs(Math.atan2((swidth[list] + lwidth[list]) / 4., rho[list]));
        }
        return dphi;
    }

    // used for cuts

    private float[] getDRho()
    {
        // only roughly correct
        // must create all
        float[] drho = new float[numData];

        // need only fill for those in draw list
        for (int i = 0; i < numDraw; i++)
        {
            int list = listdl[i];

            drho[list] = (float) (length[list] / 2.);
        }
        return drho;
    }

    protected ACoord getFZUser()
    {
        makeDrawList();
        cutArray(measuresPhi, true, " Strip");
        double[][][] hv = new double[2][2][numDraw];
        int[] index = new int[numDraw];

        for (int i = 0; i < numDraw; i++)
        {
            int list = listdl[i];

            index[i] = list;
            double deltaPhi = ((swidth[list] + lwidth[list]) / 4.) / rho[list];

            hv[0][0][i] = z[list];
            hv[1][0][i] = Math.toDegrees(phi[list] - deltaPhi);
            hv[0][1][i] = z[list];
            hv[1][1][i] = Math.toDegrees(phi[list] + deltaPhi);
        }
        return new ACoord(hv, index, this, ACoord.LINES).includePhiWrapAround("FZ");
    }

    private void makeDrawListYX()
    {
        int mode = parameterStore.get("YX", "Mode").getI();

        if (mode != AProjectionYX.MODE_NSW_STGC)
        {
            numDraw = 0;
        }
        else
        {
            makeDrawList();
        }
        cut("YX", "STGCGasGap", " STGC Gas Gap", gasGap);
    }

    protected ACoord getYXUser()
    {
        makeDrawListYX();

        double[][][] hv = new double[2][numDraw][4];
        int[] index = new int[numDraw];

		try {
		   for (int i = 0; i < numDraw; i++)
		   {
			  int list = listdl[i];

			  index[i] = list;

			  double cosPhi = Math.cos(phi[list]);
			  double sinPhi = Math.sin(phi[list]);
			  double sectphi = 2.*Math.PI / 16. * sector[list];
			  double cosSectPhi = Math.cos(sectphi);
			  double sinSectPhi = Math.sin(sectphi);
			  double rMid = rho[list];


			  if (AIdHelper.stgcChannelType(id[list]) == 0) { // pad
				 double dR = length[list] / 2.;
				 double d = swidth[list] / 2.;
				 double r = rMid;
				 double x = r * cosPhi - dR * cosSectPhi;
				 double y = r * sinPhi - dR * sinSectPhi;
				 double dx = d * sinSectPhi;
				 double dy = d * cosSectPhi;

				 hv[0][i][0] = x + dx;
				 hv[1][i][0] = y - dy;
				 hv[0][i][1] = x - dx;
				 hv[1][i][1] = y + dy;

				 x = r * cosPhi + dR * cosSectPhi;
				 y = r * sinPhi + dR * sinSectPhi;
				 d = lwidth[list] / 2.;
				 dx = d * sinSectPhi;
				 dy = d * cosSectPhi;
				 hv[0][i][2] = x - dx;
				 hv[1][i][2] = y + dy;
				 hv[0][i][3] = x + dx;
				 hv[1][i][3] = y - dy;


			  } else if (AIdHelper.stgcChannelType(id[list]) == 1) { // strip
				 double dR = swidth[list] / 2.;
				 double d = length[list] / 2.;
				 double r = rMid - dR;
				 double x = r * cosPhi;
				 double y = r * sinPhi;
				 double dx = d * sinPhi;
				 double dy = d * cosPhi;

				 hv[0][i][0] = x + dx;
				 hv[1][i][0] = y - dy;
				 hv[0][i][1] = x - dx;
				 hv[1][i][1] = y + dy;

				 r = rMid + dR;
				 x = r * cosPhi;
				 y = r * sinPhi;
				 d = length[list] / 2.;
				 dx = d * sinPhi;
				 dy = d * cosPhi;
				 hv[0][i][2] = x - dx;
				 hv[1][i][2] = y + dy;
				 hv[0][i][3] = x + dx;
				 hv[1][i][3] = y - dy;

			  } else { // wire
				 double r = rMid;
				 double dR = length[list] / 2.;
				 double d = swidth[list] / 2.;
				 double x = r * cosPhi - dR * cosSectPhi;
				 double y = r * sinPhi - dR * sinSectPhi;
				 double dx = d * sinSectPhi;
				 double dy = d * cosSectPhi;

				 hv[0][i][0] = x + dx;
				 hv[1][i][0] = y - dy;
				 hv[0][i][1] = x - dx;
				 hv[1][i][1] = y + dy;

				 x = r * cosPhi + dR * cosSectPhi;
				 y = r * sinPhi + dR * sinSectPhi;
				 d = lwidth[list] / 2.;
				 dx = d * sinSectPhi;
				 dy = d * cosSectPhi;
				 hv[0][i][2] = x - dx;
				 hv[1][i][2] = y + dy;
				 hv[0][i][3] = x + dx;
				 hv[1][i][3] = y - dy;
			  }

		   }
		   return new ACoord(hv, index, this);
		} catch (AAtlantisException e) {
		   AOutput.append("Error decoding STGC identifier: " + e.getMessage(), ALogInterface.BAD_COMMAND);
		   return ACoord.NO_DATA;
		}
    }

    protected ACoord getFRUser()
    {
        return getYXUser().convertYXToFR().includePhiWrapAround("FR");
    }

    protected ACoord getXZRZUser(int sign[]) 
    {
        double[][][] hv = new double[2][numDraw][];
        int index[] = new int[numDraw];
        
        try {
            for (int i=0; i<numDraw; i++) {
                int j = listdl[i];
                index[i] = j;

                double rho = Math.sqrt(x[j]*x[j] + y[j]*y[j]);
                
                if (AIdHelper.stgcChannelType(id[j]) == 0) { // pad
                    double drho = length[j]/2.;
                    hv[0][i] = new double[] { z[j], z[j] };
                    hv[1][i] = new double[] { sign[i] * (rho - drho),
                                              sign[i] * (rho + drho) };
				} else if (AIdHelper.stgcChannelType(id[j]) == 1) { // strip
                    hv[0][i] = new double[] { z[j] };
                    hv[1][i] = new double[] { sign[i] * rho };
                } else { // wire
                    double drho = length[j]/2.;
                    hv[0][i] = new double[] { z[j], z[j] };
                    hv[1][i] = new double[] { sign[i] * (rho - drho),
                                              sign[i] * (rho + drho) };
                }
            }

            return new ACoord(hv, index, this);
        } catch (AAtlantisException e) {
            AOutput.append("Error decoding STGC identifier: " + e.getMessage(), ALogInterface.BAD_COMMAND);
            return ACoord.NO_DATA;
        }
    }

    protected ACoord getXZUser() {
        makeDrawList();
        cutMuonSector(sector);
        
        int[] sign = new int[numDraw];
        int sect = (int) Math.round(parameterStore.get("XZ", "Phi").getD() / 22.5);
        
        for(int i=0; i<numDraw; i++) {
            if (sector[listdl[i]] == sect)
                sign[i] = 1;
            else
                sign[i] = -1;
        }
        
        return getXZRZUser(sign);
    }
    
    protected ACoord getRZUser() {
        makeDrawList();
        
        int[] sign = new int[numDraw];
        for (int i=0; i<numDraw; i++) {
            int j = listdl[i];
            sign[i] = AParameterUtilities.getRhoSign(x[j], y[j]);
        }
        
        return getXZRZUser(sign);
    }
}
