package atlantis.utils;

/**
 * 4D vector that throws exceptions when longitudinal operations are called
 * All mass is assumed to be zero for this class
 */
public class AETMis4Vector extends A4Vector
{
    public AETMis4Vector(double dx, double dy)
    {
        px = dx;
        py = dy;
    }
    

    public AETMis4Vector(A3Vector v, double m)
    {
    	throw new UnsupportedOperationException("Uses Longitudinal Components");
    }
    
    @Override
    public double getP()
    {
        throw new UnsupportedOperationException("Uses Longitudinal Components");
    }

    @Override
    public double getE()
    {
        throw new UnsupportedOperationException("Uses Longitudinal Components");
    }

    /**
     * getEt() returns the transverse energy.
     * The formula used to calculate it is:
     * Et^2 = E^2 * Pt^2 / P^2.
     */
    @Override
    public double getEt()
    {
    	double Et = this.getPt();
    	return Et;
    }
    
    @Override
    public double getMass()
    {
        return 0;
    }

    /**
     * getMt() returns the transverse mass.
     * The formula used to calculate it is:
     * Mt^2 = Et^2 - Pt^2.
     * 
     * This is from the Particle Data Group (http://pdg.lbl.gov/)
     * 
     * Note this is different to another definition in the PDG:
     * Mt^2 = E^2 - Pz^2 = m^2 + Pt^2
     *   
     * [J. Beringer et al. (Particle Data Group), Phys. Rev. D86, 010001 (2012).
     * (Kinematics, 43.6.1 and 43.5.2)]
     */
    @Override
    public double getMt()
    {
    	return 0;
    }

    @Override
    public double getPhi()
    {
    	return px == 0.0 && py == 0.0 ? 0.0 : Math.atan2(py,px);
    }

    @Override
    public double getEta()
    {
	throw new UnsupportedOperationException("Uses Longitudinal Components");
    }

    @Override
    public void add(double dx, double dy, double dz, double m)
    {
	throw new UnsupportedOperationException("Uses Longitudinal Components");
    }

    @Override
    public void add(A4Vector v)
    {
        throw new UnsupportedOperationException("Uses Longitudinal Components");
    }

    public void add(double dx, double dy)
    {
	px += dx;
	py += dy;
    }

    public void add(AETMis4Vector v)
    {
	px += v.px;
	py += v.py;
    }

    @Override
    public String toString()
    {
        return "A4Vector[Ex=" + px + ", Ey=" + py + ", Et=" + Math.sqrt(px*px + py*py) + "]";
    }

    public A4Vector make4Vector()
    {
	A4Vector v = new A4Vector();
	v.set(px, py, 0, 0);
	return v;
    }

}