package atlantis.utils;

import java.util.HashMap;

/**
 * Based on a standard Hashmap with an modified interface
 */

public class AHashMap extends HashMap
{

    private static ALogger logger = ALogger.getLogger(AHashMap.class);
    
    private void printWarningMessage(String msg)
    {
        logger.warn("AHashMap: " + msg);
    }


    public AHashMap(int num)
    {
        super(num);
    }

    public AHashMap put(String name, int i)
    {
        super.put(name, i);
        return this;
    }

    /**
     * Get float array, and log warning if it is not found.
     * @param name
     * @return array, or null if not found
     */
    public float[] getFloatArray(String name)
    {
        Object temp = get(name);
        if(temp == null)
        {
            printWarningMessage("getFloatArray(): can't find float array named " +
                                name);
        }
        return (float[]) temp;
    }

    /**
     * Get 3D float array, and log warning if it is not found.
     * @param name
     * @return array, or null if not found
     */
    public float[][][] getFloatArray3D(String name)
    {
        Object temp = get(name);

        if(temp == null)
        {
            printWarningMessage("getFloatArray3D(): can't find 3D float array named "
                                + name);
        }
        return (float[][][]) temp;
    }

    /**
     * Get string array, and log warning if it is not found.
     * @param name
     * @return array, or null if not found
     */
    public String[] getStringArray(String name)
    {
        Object temp = get(name);

        if(temp == null)
        {
            printWarningMessage("getStringArray(): can't find String array named "
                                + name);
        }
        return (String[]) temp;
    }

    /**
     * Get 2D string array, and log warning if it is not found.
     * @param name
     * @return array, or null if not found
     */
    public String[][] getStringArray2D(String name)
    {
        Object temp = get(name);

        if(temp == null)
        {
            printWarningMessage("getStringArray2D(): can't find 2D String array named "
                                + name);
        }
        return (String[][]) temp;
    }

    /**
     * Get int array, and log warning if it is not found.
     * @param name
     * @return array, or null if not found
     */
    public int[] getIntArray(String name)
    {
        Object temp = get(name);

        if(temp == null && !name.equals("sub"))
        {
            printWarningMessage("getIntArray(): can't find int array named " +
                                name);
        }
        return (int[]) temp;
    }

    /**
     * Get 2D int array, and log warning if it is not found.
     * @param name
     * @return array, or null if not found
     */
    public int[][] getIntArray2D(String name)
    {
        Object temp = get(name);

        if(temp == null)
        {
            printWarningMessage("getIntArray2D(): can't find 2D int array named " +
                                name);
        }
        return (int[][]) temp;
    }

    /**
     * Get int, and log warning (and probably crash) if it is not found .
     * @param name
     * @return value
     */
    public int getInt(String name)
    {
        Object temp = get(name);

        if(temp == null)
        {
            printWarningMessage("getInt(): can't find int named " + name);
        }
        return ((Integer) temp).intValue();
    }

    /**
     * Get long, and log warning if it is not found .
     * @param name
     * @return value, or -1 if not found
     */
    public long getLong(String name)
    {
        Object temp = get(name);

        if(temp == null)
        {
            printWarningMessage("getLong(): can't find int named " + name);
            return -1;
        }
        return ((Long) temp).longValue();
    }

    /**
     * Get float, and log warning if it is not found .
     * @param name
     * @return value, or -1 if not found
     */
    public float getFloat(String name)
    {
        Object temp = get(name);

        if(temp == null)
        {
            printWarningMessage("getFloat(): can't find int named " + name);
            return -1;
        }
        return ((Float) temp).floatValue();
    }

    /**
     * Get float array, with no warning if not found.
     * 
     * @param name
     * @return array, or array of zeroes if not found
     */
    public float[] getUnknownFloatArray(String name)
    {
        Object temp = get(name);

        if(temp == null)
            return new float[getInt("numData")];
        return (float[]) temp;
    }

    /**
     * Get long array, with no warning if not found.
     * 
     * @param name
     * @return array, or array of zeroes if not found
     */
    public long[] getUnknownLongArray(String name)
    {
        Object temp = get(name);

        if(temp == null)
            return new long[getInt("numData")];
        return (long[]) temp;
    }

    /**
     * Get int array, with no warning if not found.
     * 
     * @param name
     * @return array, or array of zeroes if not found
     */
    public int[] getUnknownIntArray(String name)
    {
        Object temp = get(name);

        if(temp == null)
            return new int[getInt("numData")];
        return (int[]) temp;
    }

    /**
     * Get float array, with no warning if not found.
     * 
     * @param name
     * @return array, or null if not found
     */
    public float[] getUnsureFloatArray(String name)
    {
        Object temp = get(name);
        return (float[]) temp;
    }

    /**
     * Get int array, with no warning if not found.
     * 
     * @param name
     * @return array, or null if not found
     */
    public int[] getUnsureIntArray(String name)
    {
        Object temp = get(name);
        return (int[]) temp;
    }

    /**
     * Get string array, with no warning if not found.
     * 
     * @param name
     * @return array, or null if not found
     */
    public String[] getUnsureStringArray(String name)
    {
        Object temp = get(name);
        return (String[]) temp;
    }
}
