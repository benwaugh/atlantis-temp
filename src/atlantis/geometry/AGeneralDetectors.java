package atlantis.geometry;

import atlantis.parameters.APar;
import atlantis.projection.AProjectionYX;
import java.util.*;

/**
 * Used to store all detectors which are not part of the muon system
 * these may be of different types.
 */

public final class AGeneralDetectors extends ADetectors {

  public AGeneralDetectors(List detectors) {
   super(filterGeneralDetectors(detectors));
  }

  private static ADetector[] filterGeneralDetectors(List detectors) {

    List tempDetectors=new ArrayList(detectors.size());
    Iterator it=detectors.iterator();

    while(it.hasNext()) {
      Object o=it.next();
      if((o instanceof ADetector)&&!(o instanceof ABoxDetector)&&!(o instanceof ATrapezoidDetector)) tempDetectors.add(o);
    }
    return (ADetector[])tempDetectors.toArray(new ADetector[tempDetectors.size()]);
  }

  private void makeDrawListYX() {
    int mode=parameterStore.get("YX", "Mode").getI();
    if(mode==AProjectionYX.MODE_STANDARD) {
      int num=0;
      for(int i=0; i<numData; ++i)
        if(det[i].getName().indexOf("FCAL")<0)
          listdl[num++]=i;
      numDraw=num;
    } 
    else if(mode>=AProjectionYX.MODE_FCAL_EM && mode<=AProjectionYX.MODE_MBTS) 
    {
      int num=0;
      for(int i=0; i<numData; ++i)
        if((mode==AProjectionYX.MODE_FCAL_EM && det[i].getName().equals("FCAL EM"))
            ||(mode==AProjectionYX.MODE_FCAL_HAD_1 && det[i].getName().equals("FCAL HAD 1"))
            ||(mode==AProjectionYX.MODE_FCAL_HAD_2 && det[i].getName().equals("FCAL HAD 2"))
            ||(mode==AProjectionYX.MODE_LAR_ENDCAP_PRESAMPLER && (det[i].getName().equals("LAr Endcap Presampler"))||det[i].getName().equals("LAr_EC_Presampler"))
            ||(mode>=AProjectionYX.MODE_LAR_ENDCAP_1 && mode<= AProjectionYX.MODE_LAR_ENDCAP_3 && (det[i].getName().equals("LAr Outer Endcap") || det[i].getName().equals("LAr Inner Endcap")))
            ||(mode>=AProjectionYX.MODE_HEC_1 && mode<= AProjectionYX.MODE_HEC_4 && det[i].getName().equals("HEC"))
            // DrawFCAL - draw FCAL inside the relevent HEC layer
            ||(parameterStore.get("YX", "DrawFCAL").getStatus() && mode==AProjectionYX.MODE_HEC_2 && det[i].getName().equals("FCAL EM"))
            ||(parameterStore.get("YX", "DrawFCAL").getStatus() && mode==AProjectionYX.MODE_HEC_3 && det[i].getName().equals("FCAL HAD 1"))
            ||(parameterStore.get("YX", "DrawFCAL").getStatus() && mode==AProjectionYX.MODE_HEC_4 && det[i].getName().equals("FCAL HAD 2"))
            ||(mode==AProjectionYX.MODE_LAR_ENDCAP_SUMMED && (det[i].getName().indexOf("LAr")>=0 || det[i].getName().indexOf("Endcap")>=0 || det[i].getName().indexOf("EC")>=0))
            ||(mode==AProjectionYX.MODE_HEC_SUMMED && det[i].getName().equals("HEC"))
            ||(mode==AProjectionYX.MODE_MBTS)&& det[i].getName().equals("Minimum Bias Trigger Scintillators"))
          listdl[num++]=i;
      numDraw=num;
    } else
      numDraw=0;
  }

  protected void makeDrawList(String projection) {
    if (projection.equals("YX") || projection.equals("FR"))
      makeDrawListYX();
    else {
      if(parameterStore.get(projection, "Mode").getI()==AProjectionYX.MODE_STANDARD)
        constructDefaultDrawList();
      else
        numDraw=0;
    }
  }

}
