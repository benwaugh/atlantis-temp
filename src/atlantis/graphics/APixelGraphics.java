package atlantis.graphics;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.geom.GeneralPath;

/**
 * This graphics is used for drawing on the screen and making pixel based
 * images. In the end all drawing is done in pixels(integers). Picking is
 * implemented here. Provide high quality pixel based drawing of thin frames
 * around lines and areas, in the method draw.
 */
// When drawing borders could speed things up by only drawing what is needed
public class APixelGraphics extends AGraphics {

    APixelGraphics(Graphics g)
    {
        super(g);
    }

    @Override
    public void setLineWidth(int lineWidth) {
        g.setStroke(new BasicStroke(lineWidth));
    }
    
    public void setColor(Color color) {
        super.setColor(color);
        g.setColor(color);
    }

    @Override
    public void drawLine(double h0, double v0, double h1, double v1) {

        GeneralPath p = new GeneralPath();
        p.moveTo(h0, v0);
        p.lineTo(h1, v1);
        
        g.draw(p);
    }
    
    public void drawSmoothLine(double h0, double v0, double ch0, double cv0,
            double ch1, double cv1, double h1, double v1) {
        
        GeneralPath p = new GeneralPath();
        p.moveTo(h0, v0);
        p.curveTo(ch0, cv0, ch1, cv1, h1, v1);

        g.draw(p);
    }

    @Override
    protected void fillRect(double h, double v, int width, int height) {

        g.fillRect((int) (h - width / 2.), (int) (v - height / 2.), width, height);
        g.drawRect((int) (h - width / 2.), (int) (v - height / 2.), width, height);
    }

    @Override
    protected void fillOval(double h, double v, int width, int height) {
        
        g.fillOval((int) (h - width / 2.), (int) (v - height / 2.), width, height);
        g.drawOval((int) (h - width / 2.), (int) (v - height / 2.), width, height);
    }
    
    @Override
    public void drawPolygon(double[] h, double[] v, int numPoints) {

        if (numPoints < 2) return;
        
        GeneralPath p = new GeneralPath();
        p.moveTo(h[0], v[0]);
        for (int i=1; i<numPoints; i++) {
            p.lineTo(h[i], v[i]);
        }
        p.closePath();
        
        g.draw(p);
    }

    @Override
    public void drawPolyline(double[] h, double[] v, int numPoints) {

        if (numPoints < 2) return;
        
        GeneralPath p = new GeneralPath();
        p.moveTo(h[0], v[0]);
        for (int i=1; i<numPoints; i++) {
            p.lineTo(h[i], v[i]);
        }
        
        g.draw(p);
    }

    @Override
    public void drawDottedPolyline(double[] h, double[] v, int numPoints) {        
        for (int i = 0; i < numPoints - 1; i += 2) {
            drawLine(h[i], v[i], h[i + 1], v[i + 1]);
        }
    }

    @Override
    public void drawSmoothPolyline(double[] h0, double[] v0, int numPoints0) {
        
        // For curves having less than 3 points and for picking use the normal polyline.
        if (numPoints0 < 3 || g instanceof APickingGraphics2D) {
            drawPolyline(h0, v0, numPoints0);
            return;
        }

        int numPoints = 3 * numPoints0 - 2;

        // Add the control points to the array, at 1/3 and 2/3
        // between the already existing points.
        float[] h = new float[numPoints];
        float[] v = new float[numPoints];
        for (int i=0; i<numPoints; i++) {

            switch (i%3) {
                case 0:
                    // One of the fixed points.
                    h[i] = (float)h0[i/3];
                    v[i] = (float)v0[i/3];
                    break;
                case 1:
                    // First control point.
                    h[i] = (float)(2./3. * h0[i/3] + 1./3. * h0[i/3+1]);
                    v[i] = (float)(2./3. * v0[i/3] + 1./3. * v0[i/3+1]);
                    break;
                case 2:
                    // Second control point.
                    h[i] = (float)(1./3. * h0[i/3] + 2./3. * h0[i/3+1]);
                    v[i] = (float)(1./3. * v0[i/3] + 2./3. * v0[i/3+1]);
                    break;
            }
        }

        // Now we have a normal polyline (straight line segments between fixed points),
        // but as a cubic Bezier curve. All we need to do to make it smooth is to move
        // the control points away from the curve, in such a way that [control point 2] -
        // [fixed point] - [control point 1] is a straight line for every fixed point.
        // We do this by averaging the angles of the line segments on either side of a
        // fixed point.
        for (int i=3; i<numPoints-2; i+=3) {
            double lenLeft = Math.sqrt(Math.pow(h[i]-h[i-1], 2) + Math.pow(v[i]-v[i-1], 2));
            double lenRight = Math.sqrt(Math.pow(h[i+1]-h[i], 2) + Math.pow(v[i+1]-v[i], 2));

            // Without length we cannot define an angle, so skip the point.
            if (lenLeft < 1e-6 || lenRight < 1e-6) continue;

            double phiLeft = Math.atan2(v[i]-v[i-1], h[i]-h[i-1]);
            double phiRight = Math.atan2(v[i+1]-v[i], h[i+1]-h[i]);

            if (phiLeft-phiRight > Math.PI) {
                phiRight += 2.*Math.PI;
            } else if (phiRight-phiLeft > Math.PI) {
                phiLeft += 2.*Math.PI;
            }

            if (Math.abs(phiLeft-phiRight) > Math.PI/2.) {
                // The line turns more than 90 degrees, this is wrong. Move the control points
                // back in so they are the same as the point on the line. This disables the smooth
                // curve locally, reverting to a normal connect-the-dots polyline.
                h[i-1] = h[i+1] = h[i];
                v[i-1] = v[i+1] = v[i];
            } else {
                // Calculate the desired angle for the tangent by weighting the angle on each side,
                // the weight is inverse proportional to the distance to the next point.
                double phi = (lenRight*phiLeft + lenLeft*phiRight) / (lenLeft+lenRight);

                h[i-1] = h[i] - (float)(lenLeft*Math.cos(phi));
                v[i-1] = v[i] - (float)(lenLeft*Math.sin(phi));
                h[i+1] = h[i] + (float)(lenRight*Math.cos(phi));
                v[i+1] = v[i] + (float)(lenRight*Math.sin(phi));
            }
        }

        for (int i=0; i<numPoints-1; i+=3) {
            drawSmoothLine(h[i], v[i], h[i+1], v[i+1], h[i+2], v[i+2], h[i+3], v[i+3]);
        }
    }
    
    @Override
    public void fillPolygon(double[] h, double[] v, int numPoints) {

        if (numPoints < 2) return;
        
        GeneralPath p = new GeneralPath();
        p.moveTo(h[0], v[0]);
        for (int i=1; i<numPoints; i++) {
            p.lineTo(h[i], v[i]);
        }
        p.closePath();
        
        g.fill(p);        
        g.draw(p);
    }
}
