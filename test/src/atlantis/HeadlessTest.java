package atlantis;

import org.junit.Test;

import org.assertj.swing.timing.Condition;
import org.assertj.swing.timing.Pause;
import org.assertj.swing.timing.Timeout;

public class HeadlessTest extends AtlantisHeadlessTestCase {

	@Test
	/** Start Atlantis in headless mode and make sure it doesn't crash part-way through.
	 *  Atlantis.initAtlantis catches all Exceptions so we check for "Atlantis Ready" in
	 *  the output console.
	 */
	public void testHeadlessMode() {
		startHeadlessMode();
		Condition ready = new Condition("output to contain \"Atlantis Ready\"") {
			public boolean test() {return stdOutCopy.toString().contains("Atlantis Ready");}
		};
		Pause.pause(ready,Timeout.timeout(10000));
	}
}
