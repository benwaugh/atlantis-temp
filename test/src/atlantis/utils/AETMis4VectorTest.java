package atlantis.utils;


import static org.junit.Assert.*;

import org.junit.Test;

/**
 * Tests for AETMis4Vector class.
 */
public class AETMis4VectorTest {
	/** Arbitrary tolerance for floating-point comparisons */
	private static final double TOLERANCE = 0.00001;

	@Test
	public void testAETMis4VectorDoubleDoubleDoubleDouble() {
		// fourth argument is mass, not energy
		// use 3-4-5 numbers for simple arithmetic
		AETMis4Vector v = new AETMis4Vector(2.0,2.0);
		assertAETMis4VectorEquals("construct 4-vector",2.0,2.0,v,TOLERANCE);
	}

	@Test(expected = UnsupportedOperationException.class)
	public void testAETMis4VectorA3VectorDouble() {
		A3Vector v3 = new A3Vector(2.0,2.0,-1.0);
		AETMis4Vector v = new AETMis4Vector(v3,4.0);
	}


	@Test(expected = UnsupportedOperationException.class)
	public void testGetP() {
		AETMis4Vector v = new AETMis4Vector(2.0,2.0);
		double p = v.getP();
	}

	@Test
	public void testGetPt() {
		AETMis4Vector v = new AETMis4Vector(0.3,0.4);
		double pt = v.getPt();
		assertEquals(0.5,pt,TOLERANCE);
	}

	@Test(expected = UnsupportedOperationException.class)
	public void testGetE() {
		AETMis4Vector v = new AETMis4Vector(2.0,2.0);
		double e = v.getE();
	}
	
	@Test
	public void testgetEt() {
		AETMis4Vector v = new AETMis4Vector(1.0,1.0);
		double et = v.getEt();
		double x = Math.sqrt(2.0);
		assertEquals(x,et,TOLERANCE);
	}

	@Test
	public void testGetMass() {
		AETMis4Vector v = new AETMis4Vector(2.0,2.0);
		double m = v.getMass();
		assertEquals(0.0,m,TOLERANCE);
	}
	
	@Test
	public void testGetMt() {
		AETMis4Vector v = new AETMis4Vector(1.0,1.0);
		double mt = v.getMt();
		assertEquals(0,mt,TOLERANCE);
	}

	@Test
	public void testAddDoubleDoubleDoubleDouble() {
		AETMis4Vector v = new AETMis4Vector(2.0,2.0);
		v.add(2.2,2.2);
		assertAETMis4VectorEquals("add 4-vectors",4.2,4.2,v,TOLERANCE);
	}

	@Test
	public void testAddAETMis4Vector() {
		AETMis4Vector v = new AETMis4Vector(2.0,-2.0);
		AETMis4Vector v2 = new AETMis4Vector(2.2,2.2);
		v.add(v2);
		assertAETMis4VectorEquals("add 4-vectors",4.2,0.2,v,TOLERANCE);
	}
	
	/**
	 * Check given AETMis4Vector has expected components
	 */
	private static void assertAETMis4VectorEquals(String message, double px, double py,
			AETMis4Vector v, double delta) {
		if (Math.abs(v.px-px)>delta || Math.abs(v.py-py)>delta) {
			String vExpected = String.format("<%f,%f,%f,%f>", px,py);
			String vActual = String.format("<%f,%f,%f,%f>", v.px,v.py);
			String summary = message+" "+
				"expected:"+vExpected+" but was:"+vActual;
			fail(summary);
		}
	}

}
