package guitest;

import org.assertj.swing.fixture.JButtonFixture;
import org.assertj.swing.fixture.JToolBarFixture;
import org.junit.Test;

public class ReadNextAndPreviousEventTest extends AtlantisGuiTestCase {

	@Test
	public void readNextThenPreviousEvent() {
		JToolBarFixture toolBarFixture = guiFixture.findEventSourceToolBar();
		JButtonFixture nextButton = toolBarFixture.button("nextButton");
		JButtonFixture previousButton = toolBarFixture.button("previousButton");
		nextButton.click();
		previousButton.click();
	}
}
