package guitest;

import static guitest.AAssert.*;

import java.io.File;

import org.assertj.swing.fixture.JComboBoxFixture;
import org.assertj.swing.fixture.JTableCellFixture;
import org.junit.Test;

/**
 * Check list of muon track collections for Trac bug #507
 * @author waugh
 *
 */
public class MuonTrackCollectionsTest extends AtlantisGuiTestCase {
	@Test
	public void loadEvents() {
		String eventsDirectory = System.getProperty("atlantis.test.events");
		guiFixture.openEventFile(new File(eventsDirectory,"muonCollections.xml"));
		checkMuonTrackCollections("ConvertedMBoyTracks",
				"ConvertedMBoyMuonSpectroOnlyTracks",
				"All");
		guiFixture.openEventFile(new File(eventsDirectory,"muonCollections2.xml"));
		checkMuonTrackCollections("ConvertedMBoyTracks",
				"ConvertedMuIdCBTracks",
				"All");
	}
	
	private void checkMuonTrackCollections(String... expected) {
		String[] contents = getMuonTrackCollections();
		assertArrayEqualsIgnoreOrder("Didn't find expected collections",expected,contents);
	}

	private String[] getMuonTrackCollections() {
		JTableCellFixture cell = guiFixture.findParameterValueCell("MuonDet","Track","Track Collections");
		cell.click();
		JComboBoxFixture comboBox = guiFrameFixture.comboBox("Track Collections");
		String[] contents = comboBox.contents();
		return contents;
	}
}
