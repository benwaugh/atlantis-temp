package guitest;

import static org.junit.Assert.fail;

import java.io.File;

import org.junit.Test;

public class LoadEventTest extends AtlantisGuiTestCase {
	@Test
	public void loadEvent() {
		String eventsDirectory = System.getProperty("atlantis.test.events");
		try {
			guiFixture.openEventFile(new File(eventsDirectory,"muonCollections.xml"));
		} catch (Exception e) {
			fail("Failed to open event: "+e);
		}
	}
}
