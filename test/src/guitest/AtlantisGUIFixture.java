package guitest;

import java.awt.Dialog;
import java.io.File;

import javax.swing.JPanel;

import org.assertj.swing.core.Robot;
import org.assertj.swing.data.TableCell;
import org.assertj.swing.driver.JComponentDriver;
import org.assertj.swing.fixture.AbstractComponentFixture;
import org.assertj.swing.fixture.DialogFixture;
import org.assertj.swing.fixture.FrameFixture;
import org.assertj.swing.fixture.JComboBoxFixture;
import org.assertj.swing.fixture.JFileChooserFixture;
import org.assertj.swing.fixture.JPanelFixture;
import org.assertj.swing.fixture.JTabbedPaneFixture;
import org.assertj.swing.fixture.JTableCellFixture;
import org.assertj.swing.fixture.JTableFixture;
import org.assertj.swing.fixture.JToolBarFixture;

public class AtlantisGUIFixture {
	private FrameFixture frameFixture;
	private Robot robot;

	public static AtlantisGUIFixture getFixture(FrameFixture guiFrameFixture) {
		return new AtlantisGUIFixture(guiFrameFixture);
	}
	
	public AtlantisGUIFixture(FrameFixture frameFixture) {
		this.frameFixture = frameFixture;
		this.robot = frameFixture.robot();
	}

	public JToolBarFixture findEventSourceToolBar() {
		return new JToolBarFixture(robot,"Atlantis event source toolbar");
	}
	
	/**
	 * Get a fixture for the interactions tool bar.
	 * @return
	 */
	public JToolBarFixture findInteractionToolBar() {
		return new JToolBarFixture(robot,"Atlantis interaction toolbar");
	}

	/** Find AParametersTable for given supergroup and group */
	protected JTableFixture findParametersTable(String superGroup, String group) {
		JTabbedPaneFixture tabs = frameFixture.tabbedPane("parameterGroups");
		tabs.selectTab(superGroup);
		JTabbedPaneFixture subTabs = frameFixture.tabbedPane(superGroup);
		subTabs.selectTab(group);
		JTableFixture parametersTable = frameFixture.table(group);
		return parametersTable;
	}

	/**
	 * Find table cell containing given parameter value.
	 * @param superGroup
	 * @param group
	 * @param parameter
	 * @return
	 */
	protected JTableCellFixture findParameterValueCell(String superGroup, String group, String parameter) {
		JTableFixture table = findParametersTable(superGroup,group);
		JTableCellFixture cellName = table.cell(parameter); // cell with name of param
		int col = cellName.column();
		int row = cellName.row();
		JTableCellFixture cellValue = table.cell(TableCell.row(row).column(col+1));
		return cellValue;
	}

	/**
	 * Open file using GUI
	 * @param file event file
	 */
	protected void openEventFile(File file) {
		frameFixture.menuItemWithPath("File", "Read Event Locally").click();
		// Assume "Open" dialog is the only one available.
		DialogFixture fixtureDialogOpen = frameFixture.dialog();
		JFileChooserFixture fileChooser = fixtureDialogOpen.fileChooser();
		fileChooser.selectFile(file);
		fileChooser.approve();
	}

}
