package guitest;

import static org.assertj.swing.finder.WindowFinder.findFrame;
import static org.assertj.swing.launcher.ApplicationLauncher.application;
import static org.junit.Assert.assertEquals;

import java.awt.Frame;
import java.io.ByteArrayOutputStream;
import java.io.PrintStream;

import org.assertj.swing.core.GenericTypeMatcher;
import org.assertj.swing.core.Robot;
import org.assertj.swing.fixture.FrameFixture;
import org.assertj.swing.junit.testcase.AssertJSwingJUnitTestCase;
import org.assertj.swing.security.NoExitSecurityManagerInstaller;
import org.junit.AfterClass;
import org.junit.BeforeClass;

import atlantis.Atlantis;
import atlantis.globals.AGlobals;
import testutils.TeeOutputStream;
import testutils.TestUtils;

/**
 * Extends the basic AssertJ Swing JUnit test case:
 * - tells AssertJ Swing to use simpleWaitForIdle to avoid long delays
 *   due to using custom event queue;
 * - start Atlantis!
 *
 */
public class AtlantisGuiTestCase extends AssertJSwingJUnitTestCase {

	protected FrameFixture       guiFrameFixture;
	protected AtlantisGUIFixture guiFixture;
	ByteArrayOutputStream stdOutCopy = new ByteArrayOutputStream();

	private static NoExitSecurityManagerInstaller noExitSecurityManagerInstaller;

	@Override
	protected void onSetUp() throws Exception {
		Robot rob = robot();
		rob.settings().simpleWaitForIdle(true);
		
		// Redirect standard output to ByteArrayOutputStream as well as normal System.out
		TeeOutputStream teeOut = new TeeOutputStream(System.out,stdOutCopy);
		System.setOut(new PrintStream(teeOut));
		
		application(Atlantis.class).start();
		
		guiFrameFixture = findFrame(new GenericTypeMatcher<Frame>(Frame.class) {
			protected boolean isMatching(Frame frame) {
				return "Atlantis GUI".equals(frame.getTitle()) && frame.isShowing();
			}
		}).using(robot());
		assertEquals("Atlantis GUI",(guiFrameFixture.target().getTitle()));
		guiFixture = AtlantisGUIFixture.getFixture(guiFrameFixture);

		String atlantisHomeDir=AGlobals.instance().getHomeDirectory();
		String testEventsDir = atlantisHomeDir+"/test/events";
		TestUtils.setPropertyIfNotSet("atlantis.test.events",testEventsDir);
	}
	
	/**
	 * Install NoExitSecurityManager because JUnit gets upset if the forked JVM exits
	 */
	@BeforeClass
	public static void setUpOnce2() {
		noExitSecurityManagerInstaller = NoExitSecurityManagerInstaller.installNoExitSecurityManager();
	}
	
	@AfterClass
	public static void tearDownOnce2() {
		noExitSecurityManagerInstaller.uninstall();
	}
}
