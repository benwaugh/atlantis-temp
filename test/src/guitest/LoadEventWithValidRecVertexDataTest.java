package guitest;

import static org.junit.Assert.*;

import java.awt.Dialog;
import java.io.File;

import org.junit.Test;

public class LoadEventWithValidRecVertexDataTest extends AtlantisGuiTestCase {
	@Test
	public void loadEvent() {
		String output = stdOutCopy.toString();
		assertTrue("Output does not contain \"Atlantis Ready\"",output.contains("Atlantis Ready"));
		String eventsDirectory = System.getProperty("atlantis.test.events");
		guiFixture.openEventFile(new File(eventsDirectory,"rvxEvent.xml"));
		output = stdOutCopy.toString();
		assertFalse("Inconsistent data incorrectly detected",output.contains("RVx: numbers of tracks are inconsistent."));
	}
}
