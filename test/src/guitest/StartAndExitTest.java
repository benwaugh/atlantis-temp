package guitest;

import static org.junit.Assert.*;

import java.awt.Frame;
import javax.swing.JButton;
import org.junit.Test;

import org.assertj.swing.core.GenericTypeMatcher;
import org.assertj.swing.core.matcher.JButtonMatcher;
import org.assertj.swing.fixture.FrameFixture;

import static org.assertj.swing.finder.WindowFinder.findFrame;

public class StartAndExitTest extends AtlantisGuiTestCase {

	@Test
	public void test() {

		guiFrameFixture.menuItemWithPath("File", "Exit").click();
		
		GenericTypeMatcher<JButton> buttonMatcherYes = JButtonMatcher.withText("Yes");
		guiFrameFixture.button(buttonMatcherYes).click();
	}
}
