package guitest;

import org.junit.Test;

import junit.framework.Assert;

import org.assertj.swing.fixture.DialogFixture;
import org.assertj.swing.fixture.JFileChooserFixture;
import org.assertj.swing.fixture.JTextComponentFixture;

public class LoadEventFailureTest extends AtlantisGuiTestCase {
	@Test
	public void loadEvent() {
		guiFrameFixture.menuItemWithPath("File", "Read Event Locally").click();
		// Assume "Open" dialog is the only one available.
		DialogFixture fixtureDialogOpen = guiFrameFixture.dialog();
		JFileChooserFixture fileChooser = fixtureDialogOpen.fileChooser();
		JTextComponentFixture fileNameBox = fileChooser.fileNameTextBox();
		fileNameBox.enterText("nosuchfile");
		fileChooser.approve();
		DialogFixture fixtureDialogInvalid = guiFrameFixture.dialog();
		String title = fixtureDialogInvalid.target().getTitle();
		Assert.assertEquals("Invalid source", title);
	}
}
