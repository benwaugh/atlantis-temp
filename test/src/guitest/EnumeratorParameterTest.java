package guitest;

import org.assertj.swing.fixture.JComboBoxFixture;
import org.assertj.swing.fixture.JTableCellFixture;
import org.junit.Test;

public class EnumeratorParameterTest extends AtlantisGuiTestCase {

	@Test
	public void test() {
		JTableCellFixture cell = guiFixture.findParameterValueCell("MuonDet","Track","Track Collections");
		cell.click();
		JComboBoxFixture comboBox = guiFrameFixture.comboBox("Track Collections");
		String[] contents = comboBox.contents();
		comboBox.selectItem("All");
	}
}
